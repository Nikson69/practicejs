const readline = require('readline');
 
const rl = readline.createInterface({
    input: process.stdin
});
 
let lines = [];
rl.on('line', (line) => {
    lines.push(line);
    if(lines.length > 1) rl.close();
}).on('close', () => {
    const [jewels, stones] = lines;
    console.log('jewels = ', jewels);
    console.log('stones = ', stones)
    let result = 0;
    for (let i = 0; i < stones.length; i++) {
        if (jewels.includes(stones.charAt(i))) {
            ++result;
        }
    }
    process.stdout.write(result.toString());
});
